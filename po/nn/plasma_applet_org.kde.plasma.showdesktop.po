# Translation of plasma_applet_org.kde.plasma.showdesktop to Norwegian Nynorsk
#
# Eirik U. Birkeland <eirbir@gmail.com>, 2008.
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2020, 2021, 2022.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2022-08-12 19:33+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/MinimizeAllController.qml:17
#, kde-format
msgctxt "@action:button"
msgid "Restore All Minimized Windows"
msgstr "Gjenopprett alle minimerte vindauge"

#: package/contents/ui/MinimizeAllController.qml:18
#, kde-format
msgctxt "@action:button"
msgid "Minimize All Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:20
#, kde-format
msgctxt "@info:tooltip"
msgid "Restores the previously minimized windows"
msgstr "Gjenopprett vindauga som vart minimerte"

#: package/contents/ui/MinimizeAllController.qml:21
#, kde-format
msgctxt "@info:tooltip"
msgid "Shows the Desktop by minimizing all windows"
msgstr ""

#: package/contents/ui/PeekController.qml:13
#, kde-format
msgctxt "@action:button"
msgid "Stop Peeking at Desktop"
msgstr "Avslutt kikking på skrivebordet"

#: package/contents/ui/PeekController.qml:14
#, kde-format
msgctxt "@action:button"
msgid "Peek at Desktop"
msgstr "Kikk på skrivebordet"

#: package/contents/ui/PeekController.qml:16
#, kde-format
msgctxt "@info:tooltip"
msgid "Moves windows back to their original positions"
msgstr "Flyttar vindauge tilbake til tidlegare plassering"

#: package/contents/ui/PeekController.qml:17
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Temporarily reveals the Desktop by moving open windows into screen corners"
msgstr ""
