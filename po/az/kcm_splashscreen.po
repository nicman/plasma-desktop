# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-10 00:47+0000\n"
"PO-Revision-Date: 2021-05-28 19:33+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.1\n"

#: kcm.cpp:160
#, kde-format
msgid "None"
msgstr "Heç biri"

#: kcm.cpp:162
#, kde-format
msgid "No splash screen will be shown"
msgstr "Açılış ekranı göstərilməyəcək"

#: kcm.cpp:180
#, kde-format
msgid "You cannot delete the currently selected splash screen"
msgstr "Cari seçilmiş açılış ekranını silə bilməzsiniz"

#: package/contents/ui/main.qml:16
#, kde-format
msgid "This module lets you choose the splash screen theme."
msgstr "Bu modul Açılış ekranı mövzusunu seçməyə imkan verir."

#: package/contents/ui/main.qml:39
#, kde-format
msgid "Failed to show the splash screen preview."
msgstr "Açılış ekranını önizləməsi göstərilə bilmədi."

#: package/contents/ui/main.qml:71
#, kde-format
msgid "Preview Splash Screen"
msgstr "Aşılış ekranına öncədən baxış"

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Uninstall"
msgstr "Silin"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "&Get New Splash Screens…"
msgstr "Yeni Açılış ekranı mövzuları &almaq…"

#. i18n: ectx: label, entry (engine), group (KSplash)
#: splashscreensettings.kcfg:12
#, kde-format
msgid "For future use"
msgstr "Gələcəkdə istifadə üçün"

#. i18n: ectx: label, entry (theme), group (KSplash)
#: splashscreensettings.kcfg:16
#, kde-format
msgid "Name of the current splash theme"
msgstr "Cari açılış mövzusunun adı"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Xəyyam Qocayev"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xxmn77@gmail.com"

#~ msgid "Splash Screen"
#~ msgstr "Açılış ekranı"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"
