# translation of plasma_applet_org.kde.plasma.kickoff.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2014, 2015, 2016, 2017, 2019.
# Mthw <jari_45@hotmail.com>, 2018.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020, 2021, 2022.
# Dusan Kazik <prescott66@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.kickoff\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-18 00:46+0000\n"
"PO-Revision-Date: 2022-06-26 14:55+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Všeobecné"

#: package/contents/ui/code/tools.js:39
#, kde-format
msgid "Remove from Favorites"
msgstr "Odstrániť z obľúbených"

#: package/contents/ui/code/tools.js:43
#, kde-format
msgid "Add to Favorites"
msgstr "Pridať do obľúbených"

#: package/contents/ui/code/tools.js:67
#, kde-format
msgid "On All Activities"
msgstr "Na všetkých aktivitách"

#: package/contents/ui/code/tools.js:117
#, kde-format
msgid "On the Current Activity"
msgstr "Na aktuálnej aktivite"

#: package/contents/ui/code/tools.js:131
#, kde-format
msgid "Show in Favorites"
msgstr "Zobraziť v obľúbených"

#: package/contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Icon:"
msgstr "Ikona:"

#: package/contents/ui/ConfigGeneral.qml:69
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Zvoliť…"

#: package/contents/ui/ConfigGeneral.qml:74
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:80
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:91
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:93
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:121
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:132
#, fuzzy, kde-format
#| msgid "General:"
msgctxt "General options"
msgid "General:"
msgstr "Všeobecné:"

#: package/contents/ui/ConfigGeneral.qml:133
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Vždy zoradiť aplikácie podľa abecedy"

#: package/contents/ui/ConfigGeneral.qml:138
#, kde-format
msgid "Use compact list item style"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:144
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:153
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Nastaviť  povolené doplnky pre vyhľadávanie…"

#: package/contents/ui/ConfigGeneral.qml:163
#, kde-format
msgid "Show favorites:"
msgstr "Zobraziť obľúbené:"

#: package/contents/ui/ConfigGeneral.qml:164
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "V mriežke"

#: package/contents/ui/ConfigGeneral.qml:172
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "V zozname"

#: package/contents/ui/ConfigGeneral.qml:180
#, kde-format
msgid "Show other applications:"
msgstr "Zobrazovať ostatné aplikácie:"

#: package/contents/ui/ConfigGeneral.qml:181
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "V mriežke"

#: package/contents/ui/ConfigGeneral.qml:189
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "V zozname"

#: package/contents/ui/ConfigGeneral.qml:201
#, kde-format
msgid "Show buttons for:"
msgstr "Zobraziť tlačidlá pre:"

#: package/contents/ui/ConfigGeneral.qml:202
#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "Power"
msgstr "Napájanie"

#: package/contents/ui/ConfigGeneral.qml:211
#, kde-format
msgid "Session"
msgstr "Sedenie"

#: package/contents/ui/ConfigGeneral.qml:220
#, kde-format
msgid "Power and session"
msgstr "Napájanie a sedenie"

#: package/contents/ui/ConfigGeneral.qml:229
#, kde-format
msgid "Show action button captions"
msgstr "Zobraziť titulky tlačidiel akcií"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Aplikácie"

#: package/contents/ui/Footer.qml:110
#, kde-format
msgid "Places"
msgstr "Miesta"

#: package/contents/ui/Header.qml:63
#, kde-format
msgid "Open user settings"
msgstr "Otvoriť používateľské nastavenia"

#: package/contents/ui/Header.qml:237
#, kde-format
msgid "Keep Open"
msgstr "Ponechať otvorené"

#: package/contents/ui/Kickoff.qml:283
#, kde-format
msgid "Edit Applications…"
msgstr "Upraviť aplikácie…"

#: package/contents/ui/KickoffGridView.qml:88
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "Mriežka s %1 riadkami, %2 stĺpcami"

#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "Leave"
msgstr "Odísť"

#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "More"
msgstr "Viac"

#: package/contents/ui/PlacesPage.qml:49
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Počítač"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "História"

#: package/contents/ui/PlacesPage.qml:51
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Často používané"

#~ msgctxt "@item:inmenu Reset icon to default"
#~ msgid "Clear Icon"
#~ msgstr "Vymazať ikonu"

#~ msgid "Search…"
#~ msgstr "Vyhľadať…"

#~ msgid "Primary actions:"
#~ msgstr "Hlavné akcie:"

#~ msgid "Power actions"
#~ msgstr "Akcie napájania"

#~ msgid "Session actions"
#~ msgstr "Akcie sedenia"

#~ msgid "Leave…"
#~ msgstr "Odísť..."

#~ msgid "Power…"
#~ msgstr "Napájanie..."

#~ msgid "More…"
#~ msgstr "Viac..."

#~ msgid "Updating applications…"
#~ msgstr "Aktualizujú sa aplikácie..."

#~ msgid "Allow labels to have two lines"
#~ msgstr "Povoliť štítku mať dva riadky"

#~ msgid "%2@%3 (%1)"
#~ msgstr "%2@%3 (%1)"

#~ msgid "%1@%2"
#~ msgstr "%1@%2"

#~ msgid "List with 1 item"
#~ msgid_plural "List with %1 items"
#~ msgstr[0] "Zoznam s 1 položkou"
#~ msgstr[1] "Zoznam s %1 položkami"
#~ msgstr[2] "Zoznam s %1 položkami"

#~ msgid "%1 submenu"
#~ msgstr "%1 podmenu"

#~ msgid "Leave..."
#~ msgstr "Odísť..."

#~ msgid "Power..."
#~ msgstr "Napájanie..."

#~ msgid "More..."
#~ msgstr "Viac..."

#~ msgid "Applications updated."
#~ msgstr "Aplikácie boli aktualizované."

#~ msgid "Switch tabs on hover"
#~ msgstr "Prepnúť karty pri prechode myšou"

#~ msgid "All Applications"
#~ msgstr "Všetky aplikácie"

#~ msgid "Favorites"
#~ msgstr "Obľúbené"

#~ msgid "Often Used"
#~ msgstr "Často používané"

#~ msgid "Active Tabs"
#~ msgstr "Aktívne karty"

#~ msgid "Inactive Tabs"
#~ msgstr "Neaktívne karty"

#~ msgid ""
#~ "Drag tabs between the boxes to show/hide them, or reorder the visible "
#~ "tabs by dragging."
#~ msgstr ""
#~ "Presuňte karty medzi schránkami na zobrazenie a skrytie alebo zmenu "
#~ "poradia viditeľných kariet."

#~ msgid "Expand search to bookmarks, files and emails"
#~ msgstr "Rozbaliť hľadanie na záložky, súbory a e-maily"

#~ msgid "Appearance"
#~ msgstr "Vzhľad"

#~ msgid "Menu Buttons"
#~ msgstr "Tlačidlá ponuky"

#~ msgid "Visible Tabs"
#~ msgstr "Viditeľné značky"

#~ msgctxt "@title:column"
#~ msgid "Favorites"
#~ msgstr "Obľúbené"

#~ msgid "Log out"
#~ msgstr "Odhlásiť sa"

#~ msgid "Lock"
#~ msgstr "Zamknúť"

#~ msgid "Lock screen"
#~ msgstr "Zamknúť obrazovku"

#~ msgid "Switch user"
#~ msgstr "Prepnúť užívateľa"

#~ msgid "Start a parallel session as a different user"
#~ msgstr "Spustiť súbežné sedenie ako iný užívateľ"

#~ msgid "Shut down"
#~ msgstr "Vypnúť"

#~ msgid "Turn off computer"
#~ msgstr "Vypnúť počítač"

#~ msgctxt "Restart computer"
#~ msgid "Restart"
#~ msgstr "Reštartovať"

#~ msgid "Restart computer"
#~ msgstr "Reštartovať počítač"

#~ msgid "Save current session for next login"
#~ msgstr "Uložiť aktuálne sedenie pre ďalšie prihlásenie"

#~ msgctxt "Puts the system on standby"
#~ msgid "Standby"
#~ msgstr "Pohotovosť"

#~ msgid "Pause without logging out"
#~ msgstr "Pozastaviť bez odhlásenia"

#~ msgid "Hibernate"
#~ msgstr "Hibernovať"

#~ msgid "Suspend to disk"
#~ msgstr "Uspať na disk"

#~ msgid "Suspend"
#~ msgstr "Uspať"

#~ msgid "Suspend to RAM"
#~ msgstr "Uspať do RAM"

#~ msgid "System"
#~ msgstr "Systém"

#~ msgid "Home Folder"
#~ msgstr "Domovský priečinok"

#~ msgid "Network Folders"
#~ msgstr "Sieťové priečinky"

#~ msgid "Documents"
#~ msgstr "Dokumenty"

#~ msgid "Recently Used Documents"
#~ msgstr "Naposledy použité dokumenty"

#~ msgid "Recently Used Applications"
#~ msgstr "Naposledy použité aplikácie"

#~ msgid "Run Command..."
#~ msgstr "Spustiť príkaz..."

#~ msgid "Run a command or a search query"
#~ msgstr "Spustiť príkaz alebo vyhľadávací dotaz"

#~ msgid "Removable Storage"
#~ msgstr "Vymeniteľné úložisko"

#~ msgid "Show 'Recently Installed'"
#~ msgstr "Zobraziť 'Nedávno nainštalované'"

#~ msgid "Uninstall"
#~ msgstr "Odinštalovať"

#~ msgid "Sort Alphabetically (Z to A)"
#~ msgstr "Triediť abecedne (od Z po A)"

#~ msgid "Clear Recent Applications"
#~ msgstr "Vyčistiť naposledy použité aplikácie"

#~ msgid "Clear Recent Documents"
#~ msgstr "Vyčistiť naposledy použité dokumenty"

#~ msgid "Sleep"
#~ msgstr "Uspať"

#~ msgid "Add To Favorites In This Activity Only"
#~ msgstr "Pridať do obľúbených iba v tejto aktivite"

#~ msgid "Plasma by KDE"
#~ msgstr "Plasma od KDE"
