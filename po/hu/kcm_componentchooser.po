# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Kristóf Kiszel <kiszel.kristof@gmail.com>, 2021.
# Kristof Kiszel <ulysses@fsf.hu>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-09 00:48+0000\n"
"PO-Revision-Date: 2022-01-14 10:58+0100\n"
"Last-Translator: Kristóf Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.07.70\n"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr "Alapértelmezett webböngésző"

#: componentchooser.cpp:74 componentchooserterminal.cpp:74
#, kde-format
msgid "Other…"
msgstr "Egyéb…"

#: componentchooserbrowser.cpp:18
#, kde-format
msgid "Select default browser"
msgstr "Válassza ki az alapértelmezett böngészőt"

#: componentchooseremail.cpp:19
#, kde-format
msgid "Select default e-mail client"
msgstr "Válassza ki az alapértelmezett e-mail klienst"

#: componentchooserfilemanager.cpp:14
#, kde-format
msgid "Select default file manager"
msgstr "Válassza ki az alapértelmezett fájlkezelőt"

#: componentchoosergeo.cpp:11
#, kde-format
msgid "Select default map"
msgstr "Válassza ki az alapértelmezett térképet"

#: componentchoosertel.cpp:15
#, kde-format
msgid "Select default dialer application"
msgstr "Válassza ki az alapértelmezett tárcsázó alkalmazást"

#: componentchooserterminal.cpp:24
#, kde-format
msgid "Select default terminal emulator"
msgstr "Válassza ki az alapértelmezett terminálemulátort"

#: package/contents/ui/main.qml:30
#, kde-format
msgid "Web browser:"
msgstr "Webböngésző:"

#: package/contents/ui/main.qml:42
#, kde-format
msgid "File manager:"
msgstr "Fájlkezelő:"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Email client:"
msgstr "E-mail kliens:"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Terminal emulator:"
msgstr "Terminálemulátor:"

#: package/contents/ui/main.qml:78
#, kde-format
msgid "Map:"
msgstr "Térkép:"

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr "Tárcsázó:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Kiszel Kristóf"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ulysses@fsf.hu"

#~ msgctxt "@title"
#~ msgid "Default Applications"
#~ msgstr "Alapértelmezett alkalmazások"

#~ msgid "Joseph Wenninger"
#~ msgstr "Joseph Wenninger"

#~ msgid "Méven Car"
#~ msgstr "Méven Car"

#~ msgid "Tobias Fella"
#~ msgstr "Tobias Fella"
