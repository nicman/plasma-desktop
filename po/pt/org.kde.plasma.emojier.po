msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2022-09-22 16:09+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Gonzalez Pol Emojis Aleix emojis\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "Selector de Emojis"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "Substituir uma instância existente"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:57
#, kde-format
msgid "Search"
msgstr "Procurar"

#: app/ui/CategoryPage.qml:74
#, kde-format
msgid "Clear History"
msgstr "Limpar o Histórico"

#: app/ui/CategoryPage.qml:153
#, kde-format
msgid "No recent Emojis"
msgstr "Sem emojis recentes"

#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "O %1 foi copiado para a área de transferência"

#: app/ui/emojier.qml:47
#, kde-format
msgid "Recent"
msgstr "Recente"

#: app/ui/emojier.qml:68
#, kde-format
msgid "All"
msgstr "Tudo"

#: app/ui/emojier.qml:76
#, kde-format
msgid "Categories"
msgstr "Categorias"

#: emojicategory.cpp:11
#, kde-format
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr "Sorrisos e Emoções"

#: emojicategory.cpp:12
#, kde-format
msgctxt "Emoji Category"
msgid "People and Body"
msgstr "Pessoas e Corpo"

#: emojicategory.cpp:13
#, kde-format
msgctxt "Emoji Category"
msgid "Component"
msgstr "Componente"

#: emojicategory.cpp:14
#, kde-format
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr "Animais e Natureza"

#: emojicategory.cpp:15
#, kde-format
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr "Comida e Bebida"

#: emojicategory.cpp:16
#, kde-format
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr "Viagens e Locais"

#: emojicategory.cpp:17
#, kde-format
msgctxt "Emoji Category"
msgid "Activities"
msgstr "Actividades"

#: emojicategory.cpp:18
#, kde-format
msgctxt "Emoji Category"
msgid "Objects"
msgstr "Objectos"

#: emojicategory.cpp:19
#, kde-format
msgctxt "Emoji Category"
msgid "Symbols"
msgstr "Símbolos"

#: emojicategory.cpp:20
#, kde-format
msgctxt "Emoji Category"
msgid "Flags"
msgstr "Bandeiras"
