# translation of kcmkded.po to
# translation of kcmkded.po to
# translation of kcmkded.po to
# translation of kcmkded.po to
# translation of kcmkded.po to
# translation of kcmkded.po to Xhosa
# Copyright (C) 2002 Free Software Foundation, Inc.
# Lwandle Mgidlana <lwandle@translate.org.za>, 2002
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-04 00:47+0000\n"
"PO-Revision-Date: 2002-10-23 12:39SAST\n"
"Last-Translator: Lwandle Mgidlana <lwandle@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"Language: xh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0beta2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Lwandle Mgidlana"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lwandle@translate.org.za"

#: kcmkded.cpp:48
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Background Services"
msgstr "Qalisa Iinkonzo"

#: kcmkded.cpp:52
#, fuzzy, kde-format
#| msgid "(c) 2002 Daniel Molkentin"
msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
msgstr "(c) 2002 Daniel Molkentin"

#: kcmkded.cpp:53
#, fuzzy, kde-format
#| msgid "(c) 2002 Daniel Molkentin"
msgid "Daniel Molkentin"
msgstr "(c) 2002 Daniel Molkentin"

#: kcmkded.cpp:54
#, kde-format
msgid "Kai Uwe Broulik"
msgstr ""

#: kcmkded.cpp:125
#, fuzzy, kde-format
msgid "Failed to stop service: %1"
msgstr "Ayikwazanga ukumisa inkonzo!"

#: kcmkded.cpp:127
#, fuzzy, kde-format
msgid "Failed to start service: %1"
msgstr "Ayikwazanga ukuqalisa inkonzo!"

#: kcmkded.cpp:134
#, fuzzy, kde-format
msgid "Failed to stop service."
msgstr "Ayikwazanga ukumisa inkonzo!"

#: kcmkded.cpp:136
#, fuzzy, kde-format
msgid "Failed to start service."
msgstr "Ayikwazanga ukuqalisa inkonzo!"

#: kcmkded.cpp:234
#, kde-format
msgid "Failed to notify KDE Service Manager (kded5) of saved changed: %1"
msgstr ""

#: package/contents/ui/main.qml:19
#, fuzzy, kde-format
msgid ""
"<p>This module allows you to have an overview of all plugins of the KDE "
"Daemon, also referred to as KDE Services. Generally, there are two types of "
"service:</p> <ul><li>Services invoked at startup</li><li>Services called on "
"demand</li></ul> <p>The latter are only listed for convenience. The startup "
"services can be started and stopped. You can also define whether services "
"should be loaded at startup.</p> <p><b>Use this with care: some services are "
"vital for Plasma; do not deactivate services if you  do not know what you "
"are doing.</b></p>"
msgstr ""
"<h1>Iinkonzo ze KDE </h1><p>Lomqongo womlinganiselo ukuvumela ukuba "
"ubenemboniselo yangaphezulu yazo zonke iplagi ezifakiweyo ze Daemon ye KDE, "
"nezaziwa Njengenkonzo ze KDE. Ngokubanzi, kuko udidi lweenkonzo ezimbini: </"
"p><ul><li>Iinkonzo ezenziweyo kuqaliso</li><li>Iinkonzo ezibizwa xa zifunwa</"
"li></ul><p>Ezokugqibela zidweliselwe uncedo kuphela kuncedo. Iinkonzo "
"zoqaliso ziyaqaliseka kwaye ziyamiseka. Kwindlela yophatho, uyakwazi "
"ukuchaza ukuba ufuna iinkonzo zilayishwe kuqaliso.</p><p><b Sebenzisa oku "
"ngenyameko. Ezinye iinkonzo zibalulekile kwi KDE. Sukuzikhupha ezinkonzo "
"ukuba awuyazi into oyenzayo!</b></p>"

#: package/contents/ui/main.qml:38
#, kde-format
msgid ""
"The background services manager (kded5) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: package/contents/ui/main.qml:47
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded5) was restarted to apply your changes."
msgstr ""

#: package/contents/ui/main.qml:98
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "Inkonzo"

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "Iyasebenza"

#: package/contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "Not running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "Ayisebenzi"

#: package/contents/ui/main.qml:136
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Startup Services"
msgstr "Qalisa Iinkonzo"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Layisha-Kwimfuno Yeenkonzo"

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Not running"
msgstr "Ayisebenzi"

#: package/contents/ui/main.qml:222
#, kde-format
msgid "Running"
msgstr "Iyasebenza"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "Inkonzo"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "Inkonzo"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "Umphathi Wenkonzo ye KDE"

#~ msgid ""
#~ "This is a list of available KDE services which will be started on demand. "
#~ "They are only listed for convenience, as you cannot manipulate these "
#~ "services."
#~ msgstr ""
#~ "Uluhlu olukhoyo lweenkonzo ze KDE ezifumanekayo ezizakuqaliswa xa "
#~ "zifuneka. zidweliswe kuphela ukunceda, njengokuba awukwazi ukwqhatha "
#~ "ezinkonzo."

#~ msgid "Status"
#~ msgstr "Isimo"

#~ msgid "Description"
#~ msgstr "Inkcazelo"

#, fuzzy
#~| msgid ""
#~| "This shows all KDE services that can be loaded on KDE startup. Checked "
#~| "services will be invoked on next startup. Be careful with deactivation "
#~| "of unknown services."
#~ msgid ""
#~ "This shows all KDE services that can be loaded on Plasma startup. Checked "
#~ "services will be invoked on next startup. Be careful with deactivation of "
#~ "unknown services."
#~ msgstr ""
#~ "Oku kubonisa zonke iinkonzo ze KDE ezilayishekayo kuqaliso lwe KDE. "
#~ "Iinkonzo ezikhangelweyo zizakwenziwa kuqaliso olulandelayo. Qhaphela xa "
#~ "ukhupha ulwenziwo lweenkonzo ezingaziwayo."

#~ msgid "Use"
#~ msgstr "Sebenzisa"

#~ msgid "Start"
#~ msgstr "Qala"

#, fuzzy
#~ msgid "Unable to contact KDED."
#~ msgstr "Ayikwazanga ukuxhumana ne KDED!"

#, fuzzy
#~ msgid "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "Ayikwazanga ukuqalisa inkonzo!"

#, fuzzy
#~ msgid "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "Ayikwazanga ukumisa inkonzo!"
