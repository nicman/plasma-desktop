# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2014, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2021-10-09 09:26-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: contents/ui/ToolBoxContent.qml:269
#, kde-format
msgid "Choose Global Theme…"
msgstr "…ਗਲੋਬਲ ਥੀਮ ਚੁਣੋ"

#: contents/ui/ToolBoxContent.qml:276
#, kde-format
msgid "Configure Display Settings…"
msgstr ""

#: contents/ui/ToolBoxContent.qml:298
#, kde-format
msgid "Exit Edit Mode"
msgstr "ਸੋਧ ਢੰਗ ਤੋਂ ਬਾਹਰ"

#~ msgid "Leave"
#~ msgstr "ਛੱਡੋ"
