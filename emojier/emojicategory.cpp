/*
    SPDX-FileCopyrightText: 2022 Weng Xuetian <wegnxt@gmail.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/
// Generated from emoji-test.txt
#include "emojicategory.h"
const QStringList &getCategoryNames()
{
    static const QStringList names = {
        I18N_NOOP2("Emoji Category", "Smileys and Emotion"),
        I18N_NOOP2("Emoji Category", "People and Body"),
        I18N_NOOP2("Emoji Category", "Component"),
        I18N_NOOP2("Emoji Category", "Animals and Nature"),
        I18N_NOOP2("Emoji Category", "Food and Drink"),
        I18N_NOOP2("Emoji Category", "Travel and Places"),
        I18N_NOOP2("Emoji Category", "Activities"),
        I18N_NOOP2("Emoji Category", "Objects"),
        I18N_NOOP2("Emoji Category", "Symbols"),
        I18N_NOOP2("Emoji Category", "Flags"),
    };
    return names;
}
